<?php

/**
 * @file
 * Contains purgecloudflarefreetier.module.
 *
 * Implements Cloudflare API v4 for free tier.
 * Purge everthing when an entity is added, updated or deleted.
 * Please note that Cloudflare does not support tag-based purging for free tier.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Implements hook_help().
 */
function purgecloudflarefreetier_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the purgecloudflarefreetier module.
    case 'help.page.purgecloudflarefreetier':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('My Awesome Module') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements Cloudflare API v4 for free tier.
 *
 * Purge everything.
 */
function purge_cloudflare_cache() {
  $config = \Drupal::config('purgecloudflarefreetier.admin_settings');

  $apiurl = "https://api.cloudflare.com/client/v4/zones/" . $config->get('cloudflare_account_zoneid') . "/purge_cache";

  $client = \Drupal::httpClient();

  try {
    $request = $client->post($apiurl, [
      'json' => [
        'purge_everything' => TRUE,
      ],
      'headers' => [
        'X-Auth-Email' => $config->get('cloudflare_account_email'),
        'X-Auth-Key' => $config->get('cloudflare_account_apikey'),
        'Content-Type' => 'application/json',
      ],
    ]);

    $response = json_decode($request->getBody());

    if ($response->success == 1) {
      if ($config->get('cloudflare_account_debugmode') == 1) {
        $message = "success = " . $response->success . ", Cloudflare cache purged.";
        \Drupal::logger('purgecloudflarefreetier')->info($message);
      }
    }
    else {
      $message = "Cloudflare cache cannot be purged. Error code(s) = " . json_encode($response->errors);
      \Drupal::logger('purgecloudflarefreetier')->warning($message);
    }
  }
  // First try to catch the GuzzleException.
  // This indicates a failed response from the remote API.
  catch (GuzzleException $error) {
    // Get the original response.
    $response = $error->getResponse();
    // Get the info returned from the remote server.
    $response_info = $response->getBody()->getContents();
    // Log the error.
    \Drupal::logger('purgecloudflarefreetier')->error($response_info);
  }
}

/**
 * Implements hook_entity_insert().
 */
function purgecloudflarefreetier_entity_insert(EntityInterface $entity) {
  purge_cloudflare_cache();
}

/**
 * Implements hook_entity_update().
 */
function purgecloudflarefreetier_entity_update(EntityInterface $entity) {
  purge_cloudflare_cache();
}

/**
 * Implements hook_entity_delete().
 */
function purgecloudflarefreetier_entity_delete(EntityInterface $entity) {
  purge_cloudflare_cache();
}
