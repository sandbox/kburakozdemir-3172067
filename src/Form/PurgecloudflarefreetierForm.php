<?php

namespace Drupal\purgecloudflarefreetier\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements admin configuration form.
 */
class PurgecloudflarefreetierForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'purgecloudflarefreetier.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'purgecloudflarefreetier_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('purgecloudflarefreetier.admin_settings');

    $form['purgecloudflarefreetier_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Cloudflare Account E-mail'),
      '#description' => $this->t('Your Cloudflare account e-mail address'),
      '#default_value' => $config->get('cloudflare_account_email'),
      '#required' => TRUE,
    ];

    $form['purgecloudflarefreetier_apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloudflare Account API Key'),
      '#description' => $this->t('Your Cloudflare account API key'),
      '#default_value' => $config->get('cloudflare_account_apikey'),
      '#required' => TRUE,
    ];

    $form['purgecloudflarefreetier_zoneid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloudflare Account Zone ID'),
      '#description' => $this->t('Your Cloudflare account Zone ID'),
      '#default_value' => $config->get('cloudflare_account_zoneid'),
      '#required' => TRUE,
    ];

    $form['purgecloudflarefreetier_debugmode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log success messages'),
      '#description' => $this->t('For debugging purposes. Log also success messages.'),
      '#default_value' => $config->get('cloudflare_account_debugmode'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('purgecloudflarefreetier.admin_settings')
      ->set('cloudflare_account_email', $form_state->getValue('purgecloudflarefreetier_email'))
      ->set('cloudflare_account_apikey', $form_state->getValue('purgecloudflarefreetier_apikey'))
      ->set('cloudflare_account_zoneid', $form_state->getValue('purgecloudflarefreetier_zoneid'))
      ->set('cloudflare_account_debugmode', $form_state->getValue('purgecloudflarefreetier_debugmode'))
      ->save();
  }

}
